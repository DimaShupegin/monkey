package com.monkey.goldenMonkey


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.webkit.CookieSyncManager
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity


class WebActivity : AppCompatActivity() {

    lateinit var webview : WebView



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)
        webview = findViewById(R.id.web_layout)
        webview.settings.javaScriptEnabled = true  //enable javaScript
        webview.setWebViewClient(WebViewClient())
        var uri = Saveuri.url
        webview.loadUrl(uri)

        webview.webViewClient = object : WebViewClient() {
            override fun onReceivedError(view: WebView, errorCode: Int, description: String, failingUrl: String) {
                //Users will be notified in case there's an error (i.e. no internet connection)
                Log.d("Cooke","Cooke = " )
            }

            override fun onPageFinished(view: WebView, url: String) {
                CookieSyncManager.getInstance().sync()
                Log.d("Cooke","Cooke = " )
            }
        }
    }

    override fun onDestroy() {
        moveTaskToBack(true);
        super.onDestroy();
        System.runFinalizersOnExit(true);
        System.exit(0);
    }


    override fun onStop() {
        super.onStop()
        finish()
    }


    override fun onBackPressed() {
        // super.onBackPressed();
        openQuitDialog()
    }


    private fun openQuitDialog() {
        val quitDialog = AlertDialog.Builder(this)
        quitDialog.setTitle("Exit")
        quitDialog.setTitle("Are you sure?")
        quitDialog.setPositiveButton("Yes") { dialog, which ->

            onDestroy()

        }
        quitDialog.setNegativeButton("No") { dialog, which -> }
        quitDialog.show()
    }
}
