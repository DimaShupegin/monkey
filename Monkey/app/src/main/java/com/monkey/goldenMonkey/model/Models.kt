package com.monkey.goldenMonkey.model

import kotlin.properties.Delegates

class Models() {

    var isfirstlaunch : Boolean = false
    lateinit var installtime: String
    lateinit var afstatus: String
    lateinit var afmessage: String
    lateinit var compaing: String
    lateinit var deeplinks: String
    lateinit var costcent: String
    lateinit var clicktime: String
    lateinit var origcost: String
    lateinit var afsub1: String
    lateinit var afsub2: String
    lateinit var afsub3: String
    lateinit var afsub4: String
    lateinit var afsub5: String

    var nametest : Boolean = false


    constructor( instal_time: String, af_status: String,  af_message: String, is_first_launch: Boolean, name_test : Boolean, deep_links : String,campaign : String, cost_cent: String,click_time: String,orig_cost : String, af_sub1:String,af_sub2:String,af_sub3:String,af_sub4:String,af_sub5:String) : this (){

        afsub1 = af_sub1
        afsub2 = af_sub2
        afsub3 = af_sub3
        afsub4 = af_sub4
        afsub5 = af_sub5
        costcent = cost_cent
        clicktime = click_time
        origcost = orig_cost
        compaing = campaign
        installtime = instal_time
        afstatus = af_status
        afmessage = af_message
        isfirstlaunch = is_first_launch
        nametest = name_test
        deeplinks = deep_links
    }
}