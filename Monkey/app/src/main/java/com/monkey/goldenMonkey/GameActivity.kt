package com.monkey.goldenMonkey

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import java.util.*
import kotlin.random.Random


class GameActivity : AppCompatActivity() {


    lateinit var startBtn: Button
    lateinit var yourpoint: TextView
    lateinit var sumtext: TextView
    lateinit var oneBtn: Button
    lateinit var twoBtn: Button
    lateinit var threeBtn: Button

    lateinit var progressBar: ProgressBar
    lateinit var imageView: ImageView

    private var progressBarStatus = 0
    var dummy: Int = 0
    var counter = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        startBtn = findViewById(R.id.start_btn)
        yourpoint = findViewById(R.id.yourpoint)
        sumtext = findViewById(R.id.summ_textview)
        oneBtn = findViewById(R.id.one_btn)
        twoBtn = findViewById(R.id.two_btn)
        threeBtn = findViewById(R.id.three_btn)
        progressBar = findViewById(R.id.progress_bar_bar)



        startBtn.setOnClickListener {
            yourpoint.visibility = View.VISIBLE
            sumtext.visibility = View.VISIBLE
            twoBtn.visibility = View.VISIBLE
            startBtn.visibility = View.INVISIBLE
            progressBar.visibility = View.VISIBLE


        }




        oneBtn.setOnClickListener {

            var ran = Random.nextInt(1, 4)
            Log.d("TTT", "ran = " + ran)
            when {
                ran == 1 -> openOneBtn()
                ran == 2 -> opentwoBtn()
                ran == 3 -> openthreeBtn()

            }


            loadProgressBar()


        }

        twoBtn.setOnClickListener {


            var ran = Random.nextInt(1, 4)
            Log.d("TTT", "ran = " + ran)
            when {
                ran == 1 -> openone()
                ran == 2 -> opentwo()
                ran == 3 -> openthree()

            }
            val summa = sumtext.text
            val ss = Integer.valueOf(summa.toString())
            val ff = ss + 1
            sumtext.setText(ff.toString())
            Log.d("DDD", "Ochki = " + summa)


            loadProgressBar()


        }

        threeBtn.setOnClickListener {
            var ran = Random.nextInt(1, 4)
            Log.d("TTT", "ran = " + ran)
            when {
                ran == 1 -> openoneBtnBtn()
                ran == 2 -> opentwoBtnBtn()
                ran == 3 -> openthreeBtnBtn()

            }
            val summa = sumtext.text
            val ss = Integer.valueOf(summa.toString())
            val ff = ss + 1
            sumtext.setText(ff.toString())
            Log.d("DDD", "Ochki = " + summa)


            loadProgressBar()
        }


    }


    private fun openthreeBtnBtn() {
        oneBtn.visibility = View.VISIBLE
        threeBtn.visibility = View.INVISIBLE

    }

    private fun opentwoBtnBtn() {
        twoBtn.visibility = View.VISIBLE
        threeBtn.visibility = View.INVISIBLE

    }

    private fun openoneBtnBtn() {
        threeBtn.visibility = View.INVISIBLE
        threeBtn.visibility = View.VISIBLE


    }

    private fun openthree() {
        threeBtn.visibility = View.VISIBLE
        twoBtn.visibility = View.INVISIBLE

    }

    private fun opentwo() {
        twoBtn.visibility = View.INVISIBLE
        twoBtn.visibility = View.VISIBLE

    }

    private fun openone() {
        oneBtn.visibility = View.VISIBLE
        twoBtn.visibility = View.INVISIBLE


    }

    private fun openOneBtn() {
        oneBtn.visibility = View.INVISIBLE
        oneBtn.visibility = View.VISIBLE

    }

    private fun opentwoBtn() {
        twoBtn.visibility = View.VISIBLE
        oneBtn.visibility = View.INVISIBLE

    }

    private fun openthreeBtn() {
        threeBtn.visibility = View.VISIBLE
        oneBtn.visibility = View.INVISIBLE

    }


    override fun onDestroy() {
        moveTaskToBack(true);
        super.onDestroy();
        System.runFinalizersOnExit(true);
        System.exit(0);
    }


    override fun onStop() {
        super.onStop()
        finish()
    }


    override fun onBackPressed() {
        // super.onBackPressed();
        openQuitDialog()
    }


    private fun openQuitDialog() {
        val quitDialog = AlertDialog.Builder(this)
        quitDialog.setTitle("Exit")
        quitDialog.setTitle("Are you sure?")
        quitDialog.setPositiveButton("Yes") { dialog, which ->

            onDestroy()

        }
        quitDialog.setNegativeButton("No") { dialog, which -> }
        quitDialog.show()
    }


    fun loadProgressBar() {
        val t = Timer()

        val tt: TimerTask = object : TimerTask() {
            override fun run() {
                counter++
                progressBar.progress = counter
                if (counter == 100) {
                    t.cancel()
                }
            }
        }
        t.schedule(tt, 0, 1000)
    }
}


