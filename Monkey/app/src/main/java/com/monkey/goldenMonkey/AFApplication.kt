package com.monkey.goldenMonkey

import android.app.Application
import android.os.Handler
import android.util.Log
import android.widget.Toast
import com.appsflyer.AppsFlyerConversionListener
import com.appsflyer.AppsFlyerLib
import com.appsflyer.AppsFlyerLibCore.LOG_TAG
import com.google.firebase.database.FirebaseDatabase
import com.monkey.goldenMonkey.model.Models
import com.monkey.goldenMonkey.singleton.Savedata
import javax.xml.transform.sax.SAXTransformerFactory


class AFApplication : Application() {

    private var AF_KEY = ""
    private var shop : String = ""
    val DL_ATTRS = "dl_attrs"

    var install_time : String = ""
    var af_status : String = ""
    var af_message : String = ""
    var tests : String = ""
    var campaign : String = ""
    var cost_cent : String = ""
    var orig_cost : String = ""
    var click_time : String = ""
    var is_first_launch : Boolean = false
    var name_test : Boolean = false
    var af_sub1 : String = ""
    var af_sub2 : String = ""
    var af_sub3 : String = ""
    var af_sub4 : String = ""
    var af_sub5 : String = ""


    private lateinit var cardinFirebase : String




    override fun onCreate() {
        super.onCreate()

        cardinFirebase = "Links"

        AF_KEY = resources.getString(R.string.AF_KEY)



        val conversionDataListener = object : AppsFlyerConversionListener {
            override fun onConversionDataSuccess(data: MutableMap<String, Any>?) {
                    if (data != null){
                        var comapy = data["campaign"]
                        Savedata.addStatus(data["af_status"].toString())
                        Log.d("GG","Company = " + comapy)
                        if(comapy != null){
                            Log.d("GG","Company = " + comapy)
                            Savedata.addCompaing(comapy as String)


                            firebase(install_time ,af_status , af_message , is_first_launch, name_test, tests, campaign, cost_cent, orig_cost,click_time,af_sub1,af_sub2,af_sub3,af_sub4,af_sub5)
                        }

                    }

                data?.map {
                    Log.d("SSS", "ConversionData: ${it.key} = ${it.value}")
                }


            }

            override fun onConversionDataFail(error: String?) {
                Log.e(LOG_TAG, "error onAttributionFailure :  $error")
            }

            override fun onAppOpenAttribution(data: MutableMap<String, String>?) {




                data?.map {
                    Log.d("SSS", "onAppOpen_attribute: ${it.key} = ${it.value}")
                }
            }

            override fun onAttributionFailure(error: String?) {
                Log.e(LOG_TAG, "error onAttributionFailure :  $error")
            }
        }



        AppsFlyerLib.getInstance().init(AF_KEY, conversionDataListener, this)

        AppsFlyerLib.getInstance().startTracking(this, AF_KEY)
    }

    private fun firebase(instal: String, afstatus: String, afmessenger: String, aflunch: Boolean, nametest: Boolean, deeplinks: String, campaign: String, clickTime: String, origcost: String, costcent: String,afsub1:String,afsub2:String,afsub3:String,afsub4:String,afsub5:String) {
        val ref = FirebaseDatabase.getInstance().getReference(cardinFirebase)
        val heroid =  ref.push().key
        val hero = heroid?.let { Models(instal,afstatus,afmessenger, aflunch,nametest,deeplinks,campaign, clickTime,origcost, costcent,afsub1,afsub2,afsub3,afsub4,afsub5) }

        if (heroid != null) {
            ref.child(heroid).setValue(hero).addOnCompleteListener {


            }
        }
    }


}






//
//    private fun loadShop() : Boolean {
//        var loadShoptrue = false
//        shop =  AppsFlyerLib.getInstance().setOutOfStore("example_store").toString()
//        Log.d("GG", "Shop 1 == " +  shop)
//        if (shop == "kotlin.Unit"){
//            loadShoptrue = true
//        }else{
//            loadShoptrue = false
//        }
//
//        return  loadShoptrue
//    }