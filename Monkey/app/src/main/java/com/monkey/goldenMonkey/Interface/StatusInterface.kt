package com.monkey.goldenMonkey.Interface

interface StatusInterface {
    fun onStatusChange(status: String)
}